<?php
//Valida que la solicitud sea POST
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$_POST= json_decode(file_get_contents('php://input'), true); //Decodifica el formato JSON
	$input = $_POST; //Guardamos el POST decodificado en la variable input
	$token=$input['token'];
	$codigo=$input['codigo'];
	$valor=$input['valor'];

	if($token==''||$token==null || $codigo==''||$codigo==null || $valor==''||$valor==null)
	{
		echo "HTTP/1.1 400 Bad Request";
	}
	else 
	{
		if($valor=='1')
		{
			//Estado encendido (reemplazar comando deseado funciona ne Linux y Windows)
			$comando1='ping -4 google.com';
			$salida1 = shell_exec($comando1);
			echo $salida1;
		}
		else if($valor=='0')
		{   //Estado apagado (reemplazar comando deseado)
			$comando2='ping -6 google.com';
			$salida2 = shell_exec($comando2);
			echo $salida2;
		}
		else
		{
			echo "HTTP/1.1 400 Bad Option " . $valor;
		}
	
	}
}
else
{
	echo "HTTP/1.1 400 Bad Method";
}
?>
